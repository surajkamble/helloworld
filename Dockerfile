FROM openjdk:8

ADD target/HelloWorld-0.0.1-SNAPSHOT.jar hello-world.jar

ENTRYPOINT ["java", "-jar", "hello-world.jar"]

EXPOSE 5000