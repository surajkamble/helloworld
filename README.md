# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Jenkins CICD pipeline 

Jenkins pipeline 

Backend: - Java maven project
Tools:- Docker, SonarQube server, Jenkins on Local machine, jenkins pipeline, EC2

pipeline consists  stages

stage 1: Pull latest Code
           |
stage 2: Build Java Application
           |
stage 3: Run Unit test cases
           |
stage 4: Code Quality Check via SonarQube
           |
stage 5: Build java Docker Images
           |
stage 6: Push Docker image to the Docker Hub
           |
stage 7: Deploy hello world app on EC2

I have used Jenkins Pipeline for HelloWorld application. I have used Jenkinsfile which is present in Root directory of code

Description - 

1. Pull latest Code - 
It will Pull latest code when developer commit code

2. Build Java Application - 
It will build Helloworld app using maven command i.e mvn clean -B install

3. Run Unit test cases - 
It will run unit test cases using i.e mvn test

4. Code Quality Check via SonarQube - 
It will anaylsis java code and send report to the sonarqube server.
I have installed Sonarqube server on ec2 using docker.
SonarQube tool can be used for code inspection and inspect both the source code and the compiled code. SonarQube is used to check code quality and analysis of code to detect bugs, code smells, and security vulnerabilities. 
SonarQube works with 20+ programming languages and can also generate reports on duplicated code, coding standards, unit tests, and code coverage.

5. Build java Docker Images - 
It will build a hello-world java project using dockerfile and tagged with the "BuildNumber"

6. Push Docker image to the Docker Hub - 
After Successful building docker image it will push docker image to the docker hub repository

7. Deploy hello world app on EC2 - 
On jenkins, It will ssh to the EC2 machine using ssh-agent plugin and Deploy helloworld app with latest updated version
On ec2 there is deploy.sh script it will stop previous container and Pull latest image code from docker hub and using sed command i am replacing latest docker image tag and up container with latest image.

You can find Application and Sonarqube URL on:- 

SonarQube url - http://13.233.99.43:9000/
username- admin
password- admin

App url - http://13.233.99.43:5000/HelloWorldExample/hello




           

